package org.academiadecodigo.tailormoons.exceptionsexercise;

import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.FileNotFoundException;
import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.NotEnoughSpaceException;
import org.academiadecodigo.tailormoons.exceptionsexercise.manager.FileManager;

public class Main {

    public static void main(String[] args) {

        FileManager fileManager = new FileManager(3);

        fileManager.login();

        try {
            fileManager.createFile(".exe");
            fileManager.createFile(".pdf");
            fileManager.createFile(".wav");
            fileManager.createFile(".exe");
        } catch (NotEnoughSpaceException | NotEnoughPermissionsException s) {
            System.out.println(s.getMessage());
        }



        try {
            fileManager.getFile(".exe");
        } catch (FileNotFoundException | NotEnoughPermissionsException s) {
            System.out.println(s.getMessage());
        }


        fileManager.logout();


    }
}
