package org.academiadecodigo.tailormoons.exceptionsexercise.manager;

public class File {


    private String name;


    public File(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
