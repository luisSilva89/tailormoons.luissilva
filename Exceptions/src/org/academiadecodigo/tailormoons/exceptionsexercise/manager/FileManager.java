package org.academiadecodigo.tailormoons.exceptionsexercise.manager;

import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.NotEnoughSpaceException;
import org.academiadecodigo.tailormoons.exceptionsexercise.exceptions.FileNotFoundException;


public class FileManager {

    private boolean logged = false;
    private File[] file;
    private int space;


    public FileManager(int space) {
        this.file = new File[space];
        this.space = space;
    }


    public void login() {
        logged = true;
        System.out.println("Login successful. Welcome");
    }

    public void logout() {
        logged = false;
        System.out.println("Logout successful. Goodbye");
    }

    public void createFile(String name) throws NotEnoughPermissionsException, NotEnoughSpaceException {

        if (!logged) {
            throw new NotEnoughPermissionsException("Login first");
        }

        for (int i = 0; i < space ; i++) {
            if (file[i] == null) {
                file[i] = new File(name);
                System.out.println("File creation successful" + " " + name);
                return;
            }
            
        }
        throw new NotEnoughSpaceException("There is no space available");

    }


    public String getFile(String name) throws NotEnoughPermissionsException, FileNotFoundException {

        if (!logged) {
            throw new NotEnoughPermissionsException("Login first");
        }

        for (int i = 0; i < file.length; i++) {
            if (file[i].getName() != name) {
                continue;
            }
            System.out.println("Here, take your file");
            return file[i].getName();
        }
        throw new FileNotFoundException("File does not exist");

    }

}
