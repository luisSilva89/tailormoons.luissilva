package org.academiadecodigo.tailormoons.exceptionsexercise.exceptions;

public class NotEnoughSpaceException extends FileException {

    public NotEnoughSpaceException(String s) {
        super(s);

    }

}
