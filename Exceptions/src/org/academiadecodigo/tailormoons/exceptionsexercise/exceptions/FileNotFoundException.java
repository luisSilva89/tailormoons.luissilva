package org.academiadecodigo.tailormoons.exceptionsexercise.exceptions;

public class FileNotFoundException extends FileException {

    public FileNotFoundException(String s) {
        super(s);
    }

}
