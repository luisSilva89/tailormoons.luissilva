package org.academiadecodigo.tailormoons.exceptionsexercise.exceptions;

public class NotEnoughPermissionsException extends FileException {

    public NotEnoughPermissionsException(String s) {
        super(s);

    }

}
