package org.academiadecodigo.tailormoons.exceptionsexercise.exceptions;

public class FileException extends Exception {


    public FileException(String s) {
        super(s);
    }

}
