package org.academiadecodigo.tailormoons;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Main {

    public static void main(String[] args) throws IOException {


        FileAnalyzer fileAnalyzer = new FileAnalyzer();

        System.out.println(fileAnalyzer.countWords(Path.of("assets/docText1")));

        fileAnalyzer.firstLongerWord(Path.of("assets/docText1"), 5).ifPresent(System.out::println);
        System.out.println(fileAnalyzer.firstLongerWord(Path.of("assets/docText1"), 4).get());

    }
}
