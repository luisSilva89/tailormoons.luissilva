package org.academiadecodigo.tailormoons;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class FileAnalyzer<T> {


    public long countWords(Path filePath) throws IOException {

        try {

            return Files.lines(filePath)
                    //Stream<String> to Stream<String[]> for each line received (Stream holds a biDimensional Array [line][words])
                    .map(line -> line.split(" "))
                    //flatMap returns a stream with the content of the arrays (String words)
                    .flatMap(words -> Arrays.stream(words))
                    .count();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public Optional<String> firstLongerWord(Path filePath, int n) throws IOException {
        return Files.lines(filePath)
                .map(line -> line.split(" "))
                .flatMap(Arrays::stream)
                .filter(word -> word.length() > n)
                .findFirst();
    }


    public String longestWords(Path filePath, int qty) throws IOException {
        return Files.lines(filePath)
                .map(line -> line.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingInt(word -> word.length()))
                .limit(qty)
                .collect(Collectors.joining(" "));
    }


    public String[] commonWords(Path filePath, Path otherFilePath) {
        return null;
    }


}
