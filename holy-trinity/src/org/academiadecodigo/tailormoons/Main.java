package org.academiadecodigo.tailormoons;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {


        String message = "I'll send an SOS to the garbage world, " +
                "I hope that someone garbage gets my message in a garbage bottle.";

        String[] newMessage = message.split(" ");

        String a = new String("luis");
        String b = new String("luis");
        System.out.println(a == b); //False
        System.out.println(a.equals(b)); //True

        String finalMessage = Stream.of(newMessage).filter(word -> !word.equals("garbage"))
                .map(String::toUpperCase)
                .collect(Collectors.joining(" "));

        System.out.println(finalMessage);


        //Alternative method
        String finalMessage1 = Stream.of(message.split(" "))
                .filter(word -> !word.equals("garbage"))
                .map(String::toUpperCase)
                .reduce("Sid said: ", (sentence, word) -> sentence + " " + word);

        System.out.println((finalMessage1).trim());

    }

}
