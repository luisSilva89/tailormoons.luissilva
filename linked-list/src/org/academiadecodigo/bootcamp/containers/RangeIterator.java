package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;

public class RangeIterator<T> implements Iterator<T> {


    private final LinkedList<T> linkedList;
    private int min;
    private int max;
    private int index;

    public RangeIterator(LinkedList<T> linkedList, int min, int max) {
        this.min = min;
        this.max = max;
        this.index = min;
        this.linkedList = linkedList;
    }


    @Override
    public T next() {
        index++;
        return linkedList.get(index - 1);
    }

    public boolean hasNext() {
        if (index > max) {
            return false;
        }
        return linkedList.get(index) != null;
    }

}
