package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;

public class Range implements Iterable<Integer> {


    private LinkedList<Integer> linkedList;
    private int min;
    private int max;

    public Range(int min, int max, LinkedList<Integer> linkedList) {
        this.min = min;
        this.max = max;
        this.linkedList = linkedList;

    }


    @Override
    public Iterator<Integer> iterator() {

        return new RangeIterator<>(linkedList, min, max);

    }
}
