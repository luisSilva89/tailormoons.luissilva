package org.academiadecodigo.tailormoons.udpuppercase;

import java.io.IOException;
import java.net.*;

public class Server {

    public static void main(String[] args) {


        try {
            DatagramSocket socket = new DatagramSocket(8888);

            byte[] buffer = new byte[100];

            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            socket.receive(packet);

            String message = new String(buffer);

            System.out.println(message.toUpperCase());


        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
