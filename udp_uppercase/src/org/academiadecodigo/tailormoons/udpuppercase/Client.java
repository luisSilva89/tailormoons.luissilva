package org.academiadecodigo.tailormoons.udpuppercase;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Client {


    public static void main(String[] args) {


        String message = "hola";

        try (DatagramSocket clientSocket = new DatagramSocket()) {

            byte[] buffer = message.getBytes();
            clientSocket.setSoTimeout(3000);

            InetAddress ip = InetAddress.getByName("localhost");
            int port = 8888;

            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, ip, port);

            clientSocket.send(packet);

        } catch (SocketException e) {
            System.out.println(e);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
