package org.academiadecodigo.tailormoons.udpuppercase.server;

import java.io.IOException;
import java.net.*;

public class Server {


    private DatagramSocket serverSocket;
    private DatagramPacket rcvPacket;
    private InetAddress clientAddress;
    private int clientPort;
    byte[] rcvBuffer = new byte[1024];
    byte[] sndBuffer;
    private String reply;


    public void receiveData() {

        try {
            serverSocket = new DatagramSocket(8888);
            rcvPacket = new DatagramPacket(rcvBuffer, rcvBuffer.length);

            serverSocket.receive(rcvPacket);

            String string = new String(rcvPacket.getData());
            reply = string.toUpperCase();

            clientAddress = rcvPacket.getAddress();
            clientPort = rcvPacket.getPort();

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void sendData() {

        try {

            sndBuffer = reply.getBytes();

            DatagramPacket sndPacket = new DatagramPacket(sndBuffer, sndBuffer.length, clientAddress, clientPort);
            serverSocket.send(sndPacket);
            serverSocket.close();

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {


        Server server = new Server();

        server.receiveData();
        server.sendData();

    }


}
