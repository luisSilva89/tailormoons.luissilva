package org.academiadecodigo.tailormoons.udpuppercase.client;

import java.io.IOException;
import java.net.*;

public class Client {

    private String message = "holla";
    private DatagramSocket clientSocket;
    byte[] rcvBuffer = new byte[1024];
    byte[] sndBuffer;

    public Client() {
        init();
    }

    private void init() {
        try {
            clientSocket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void sendData() {

        try {
            sndBuffer = message.getBytes();
            InetAddress ip = InetAddress.getByName("localhost");
            int port = 8888;

            DatagramPacket sndPacket = new DatagramPacket(sndBuffer, sndBuffer.length, ip, port);
            clientSocket.send(sndPacket);

        } catch (UnknownHostException e) {
            System.err.println("Host doesn't exist");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Error sending packet");
            e.printStackTrace();
        }
    }


    public void receiveData() {

        try {
            DatagramPacket rcvPacket = new DatagramPacket(rcvBuffer, rcvBuffer.length);

            clientSocket.receive(rcvPacket);

            String receivedMessage = new String(rcvPacket.getData());

            System.out.println(receivedMessage);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {


        Client client = new Client();

        client.sendData();
        client.receiveData();

    }

}
