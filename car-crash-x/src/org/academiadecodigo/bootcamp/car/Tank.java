package org.academiadecodigo.bootcamp.car;

import org.academiadecodigo.bootcamp.grid.position.GridPosition;

public class Tank extends Car {

    public final static int SPEED = 1;
    private final static int MOVES_BEFORE_BREAK = 30;
    private final static int BREAK_INTERVAL = 4;
    private int moves = 0;

    /**
     * Constructs a brand new Fiat
     *
     * @param pos the position of the car in the grid
     */
    public Tank(GridPosition pos) {
        super(pos, CarType.TANK);
    }

    /**
     * @see Car#move()
     */
    @Override
    public void move() {

        moves++;

        if (moves < MOVES_BEFORE_BREAK || moves % BREAK_INTERVAL != 0) {
            accelerate(chooseDirection(), Tank.SPEED);
        }

    }

}
