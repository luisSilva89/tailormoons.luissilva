package org.academiadecodigo.tailormoons.simplewebserver;

import java.io.IOException;

public class ServerLauncher {

    public static void main(String[] args) {

        int portNumber = 8080;

        try {
            Server server = new Server(portNumber);
            server.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
