package org.academiadecodigo.tailormoons.simplewebserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {


    private final ServerSocket serverSocket;


    public Server(int portNumber) throws IOException {

        serverSocket = new ServerSocket(portNumber);
        System.out.println("Server Listening on port: " + portNumber);

    }


    public void start() {

        while (true) {

            try {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Connected to: " + clientSocket.toString());
                System.out.println();

                Thread client = new Thread(new ClientHandler(clientSocket));
                client.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
