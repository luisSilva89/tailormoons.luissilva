package org.academiadecodigo.tailormoons.simplewebserver;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {


    private Socket clientSocket;
    private OutputStream outputStream;
    private BufferedReader inputStream;
    private File file;


    public ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        start();
    }

    public void start() {

        try {
            getClientRequest();

            clientSocket.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
            //e.printStackTrace();
        } catch (InvalidRequestException e) {
            System.err.println("Invalid Request Header");
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getClientRequest() throws IOException, InvalidRequestException {

        inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String request = inputStream.readLine();

        System.out.println(request);

        if (request == null) {
            throw new IOException("Client Disconnected forcefully");
        }

        String[] clientRequest = request.split(" ");

        if (clientRequest.length < 2) {
            throw new InvalidRequestException();
        }
        if (clientRequest[0].equals("GET")) {
            sendData(clientRequest[1]);
        }

    }

    public void sendData(String fileName) throws IOException {

        outputStream = clientSocket.getOutputStream();

        if (fileName.equals("/")) {
            fileName = "index.html";
        }

        file = new File("www/" + fileName);

        if (!file.exists()) {
            file = new File("www/404.html");
            long size = file.length();

            String head = "HTTP/1.0 404 Not Found\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length:" + size + "\r\n" +
                    "\r\n";

            outputStream.write(head.getBytes());

            readFile();
        }
        long size = file.length();

        String head = "HTTP/1.0 200 Document Follows\r\n" +
                "Content-Type: text/html; charset=UTF-8\r\n" +
                "Content-Length:" + size + "\r\n" +
                "\r\n";

        outputStream.write(head.getBytes());

        readFile();
    }


    public void readFile() throws IOException {

        FileInputStream inputFile = new FileInputStream(file);

        byte[] buffer = new byte[1024];
        int numberOfBytesToWrite = 0;

        while ((numberOfBytesToWrite = inputFile.read(buffer)) != -1) {
            outputStream.write(buffer);
        }
    }
}
