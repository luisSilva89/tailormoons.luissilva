package org.academiadecodigo.tailormoons.battlesimulation.fighters;
import org.academiadecodigo.tailormoons.battlesimulation.Utilities;

public class Wizard extends Fighter {

    private static final int ATTACK_DAMAGE = 7;
    private static final int SPELL_DAMAGE = 50;
    private static final int INITIAL_HEALTH = 90;
    private int life = INITIAL_HEALTH;
    private boolean shield;


    public Wizard(String name) {
        super(name);
    }

    public void hit(Fighter opponent) {
        System.out.println(this.getName() + " hit (" + ATTACK_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(ATTACK_DAMAGE);
    }

    @Override
    public void cast(Fighter opponent) {
        if(Utilities.generateNumber(2) == 1) {
            System.out.println(this.getName() + " placed a shield. He will not suffer damage next turn");
            shield = true;
            return;
        }
        System.out.println(this.getName() + " cast a spell (" + SPELL_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(SPELL_DAMAGE);
    }

    @Override
    public void suffer(int damage) {
        if(shield) {
            System.out.println(this.getName() + " has a shield. He takes no damage this turn");
            shield = false;
            return;
        }
        life = life - damage;
    }

    @Override
    public boolean isDead() {
        if(life < 0) {
            return true;
        }
        return false;
    }

    private int getLife() {
        return life;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getName());
        stringBuilder.append(" has ");
        stringBuilder.append(getLife());
        stringBuilder.append(" remaining life. ");
        return stringBuilder.toString();
    }
}
