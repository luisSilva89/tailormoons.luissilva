package org.academiadecodigo.tailormoons.battlesimulation.fighters;


public abstract class Fighter {

    private String name;

    public Fighter(String name) {
        this.name = name;
    }

    public abstract void hit(Fighter opponent);

    public abstract void cast(Fighter opponent);

    public abstract void suffer(int damage);

    public abstract boolean isDead();

    public String getName() {
        return name;
    }
}
