package org.academiadecodigo.tailormoons.battlesimulation.fighters;
import org.academiadecodigo.tailormoons.battlesimulation.Utilities;

public class Troll extends Fighter {

    private static final int ATTACK_DAMAGE = 30;
    private static final int SPELL_DAMAGE = 10;
    private static final int INITIAL_HEALTH = 300;
    private int life = INITIAL_HEALTH;


    public Troll(String name) {
        super(name);
    }

    @Override
    public void hit(Fighter opponent) {
        if(Utilities.generateNumber(2) == 1) {
            System.out.println(this.getName() + " is napping, he cannot attack.");
            return;
        }
        System.out.println(this.getName() + " hit (" + ATTACK_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(ATTACK_DAMAGE);
    }

    @Override
    public void cast(Fighter opponent){
        System.out.println(this.getName() + " cast a spell (" + SPELL_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(SPELL_DAMAGE);
    }

    @Override
    public void suffer(int damage) {
        life = life - damage;
    }

    @Override
    public boolean isDead() {
        if(life < 0) {
            return true;
        }
        return false;
    }

    private int getLife() {
        return life;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getName());
        stringBuilder.append(" has ");
        stringBuilder.append(getLife());
        stringBuilder.append(" remaining life. ");
        return stringBuilder.toString();
    }
}
