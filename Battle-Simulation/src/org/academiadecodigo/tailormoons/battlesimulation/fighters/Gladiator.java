package org.academiadecodigo.tailormoons.battlesimulation.fighters;
import org.academiadecodigo.tailormoons.battlesimulation.Utilities;

public class Gladiator extends Fighter {

    private static final int ATTACK_DAMAGE = 40;
    private static final int SPELL_DAMAGE = 7;
    private static final int INITIAL_HEALTH = 150;
    private int life = INITIAL_HEALTH;


    public Gladiator(String name) {
        super(name);
    }

    @Override
    public void hit(Fighter opponent) {
        if(Utilities.generateNumber(3) == 2) {
            System.out.println(this.getName() + " lands a critical strike! (" + ATTACK_DAMAGE*2 + "dmg) to " + opponent.getName());
            opponent.suffer(ATTACK_DAMAGE*2);
            return;
        }
        System.out.println(this.getName() + " hit (" + ATTACK_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(ATTACK_DAMAGE);
    }

    @Override
    public void cast(Fighter opponent) {
        System.out.println(this.getName() + " cast a spell (" + SPELL_DAMAGE + "dmg) to " + opponent.getName());
        opponent.suffer(SPELL_DAMAGE);
    }

    @Override
    public void suffer(int damage) {
        life = life - damage;
    }

    @Override
    public boolean isDead() {
        if(life < 0) {
            return true;
        }
        return false;
    }

    private int getLife() {
        return life;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getName());
        stringBuilder.append(" has ");
        stringBuilder.append(getLife());
        stringBuilder.append(" remaining life. ");
        return stringBuilder.toString();
    }
}
