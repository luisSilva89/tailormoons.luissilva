package org.academiadecodigo.tailormoons.battlesimulation;
import org.academiadecodigo.tailormoons.battlesimulation.fighters.Fighter;

public class Player {

    private String name;
    private Fighter[] fighters;

    public Player(String name, Fighter[] fighters) {
        this.name = name;
        this.fighters = fighters;
    }

    public String getName() {
        return name;
    }

    public void attack(Player opponent) {

        if(lost() || opponent.lost()) {
            return;
        }

        int number = Utilities.generateNumber(2);
        if (number == 0) {

            int numberOne = Utilities.generateNumber(3);
            while (fighters[numberOne].isDead()) {
                numberOne = Utilities.generateNumber(3);
            }
            int numberTwo = Utilities.generateNumber(3);
            while (opponent.fighters[numberTwo].isDead()) {
                numberTwo = Utilities.generateNumber(3);
            }
            fighters[numberOne].hit(opponent.fighters[numberTwo]);
            System.out.println(opponent.fighters[numberTwo]);
            return;
        }
        int numberOne = Utilities.generateNumber(3);
        while (fighters[numberOne].isDead()) {
            numberOne = Utilities.generateNumber(3);
        }
        int numberTwo = Utilities.generateNumber(3);
        while (opponent.fighters[numberTwo].isDead()) {
            numberTwo = Utilities.generateNumber(3);
        }
        fighters[numberOne].cast(opponent.fighters[numberTwo]);
        System.out.println(opponent.fighters[numberTwo]);
        }

    public boolean lost() {

        if((fighters[0].isDead() && fighters[1].isDead() && fighters[2].isDead())) {
            return true;
        }
        return false;
    }
}
