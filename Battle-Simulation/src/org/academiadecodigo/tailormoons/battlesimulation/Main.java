package org.academiadecodigo.tailormoons.battlesimulation;

import org.academiadecodigo.tailormoons.battlesimulation.fighters.Fighter;
import org.academiadecodigo.tailormoons.battlesimulation.fighters.Troll;
import org.academiadecodigo.tailormoons.battlesimulation.fighters.Wizard;
import org.academiadecodigo.tailormoons.battlesimulation.fighters.Gladiator;

public class Main {

    public static void main(String[] args) {


        Fighter[] luisFighters = {
                new Troll("Snow Troll"),
                new Troll("Cave Troll"),
                new Gladiator("Spartacus")
        };

        Fighter[] someoneFighters = {
                new Wizard("Saruman"),
                new Wizard("Gandalf"),
                new Gladiator("Aragorn")
        };

        Player luis = new Player("Luis", luisFighters);
        Player someone = new Player("Some1", someoneFighters);

        Arena arena = new Arena(luis, someone);

        arena.battle();
    }
}
