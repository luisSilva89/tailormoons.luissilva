package org.academiadecodigo.tailormoons.battlesimulation;

public class Arena {

    private Player playerOne;
    private Player playerTwo;


    public Arena(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }


    public void battle() {

        System.out.println("GAME STARTING: " + playerOne.getName() + " VS " + playerTwo.getName());
        System.out.println();
        int counter = 1;

        while(!playerOne.lost() && !playerTwo.lost()) {
            System.out.println("=========NEW ROUND " + counter + "==========");
            counter++;
            System.out.println(">> " + playerOne.getName() + "'s turn to attack " + playerTwo.getName() + " <<");
            playerOne.attack(playerTwo);

            System.out.println(">> " + playerTwo.getName() + "'s turn to attack " + playerOne.getName() + " <<");
            playerTwo.attack(playerOne);
            System.out.println();
        }

        if(playerOne.lost()) {
            System.out.println(playerOne.getName() + " has no fighters alive...");
            System.out.println();
            System.out.println(playerTwo.getName() + " has won the game!");
            return;
        }
        System.out.println(playerTwo.getName() + " has no fighters alive...");
        System.out.println();
        System.out.println(playerOne.getName() + " has won the game!");
        return;
    }
}
