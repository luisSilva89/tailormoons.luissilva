package org.academiadecodigo.tailormoons.outerrange;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        Range range = new Range(5, 10);


        range.setForward(false);


        for (Integer i : range) {
            System.out.println("Iterated: " + i);
        }

    }

}
