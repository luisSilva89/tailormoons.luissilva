package org.academiadecodigo.tailormoons.outerrange;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class Range implements Iterable<Integer> {

    private int min;
    private int max;
    private boolean forward;


    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }


    @Override
    public Iterator<Integer> iterator() {

        if (forward) {

            return new Iterator<>() {

                int current = min - 1;

                @Override
                public boolean hasNext() {
                    while ((current + 1) < max) {

                        return current < max;
                    }

                    return current < max;
                }

                @Override
                public Integer next() {
                    if (!hasNext()) {
                        throw new NoSuchElementException();
                    }
                    current++;

                    return current;
                }
            };
        }


        return new Iterator<>() {

            int current = max + 1;

            @Override
            public boolean hasNext() {

                return current > min;

            }

            @Override
            public Integer next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                current--;

                return current;
            }
        };
    }
}
