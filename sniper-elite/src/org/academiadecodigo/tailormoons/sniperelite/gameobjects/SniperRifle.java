package org.academiadecodigo.tailormoons.sniperelite.gameobjects;

import org.academiadecodigo.tailormoons.sniperelite.Utilities;

public class SniperRifle {


    private final int bulletDamage;
    private int bulletsFired;

    public SniperRifle(int bulletDamage) {
        this.bulletDamage = bulletDamage;
    }

    public void shoot(Destroyable enemy) {

        while (!enemy.isDestroyed()) {

            int criticalStrike = Utilities.generateNumber(3);
            if(criticalStrike == 3) {
                System.out.println("You land a critical strike! (" + bulletDamage*2 + "dmg)");
                enemy.hit(bulletDamage*2);
                bulletsFired++;
            }
            System.out.println("You land a shot! (" + bulletDamage + "dmg)");
            enemy.hit(bulletDamage);
            bulletsFired++;
        }
    }

    public int getBulletsFired() {
        return bulletsFired;
    }

}
