package org.academiadecodigo.tailormoons.sniperelite.gameobjects;

public interface Destroyable {

    void hit(int damage);

    boolean isDestroyed();

}
