package org.academiadecodigo.tailormoons.sniperelite.gameobjects;


public class SoldierEnemy extends Enemy {

    private static final String NAME = "FOOT_SOLDIER";

    public SoldierEnemy(String name, int health) {
        super(name, health);
    }

    @Override
    public String getName() {
        return NAME;
    }


}
