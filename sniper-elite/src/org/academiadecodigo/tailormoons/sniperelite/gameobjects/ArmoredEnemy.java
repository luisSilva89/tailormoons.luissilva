package org.academiadecodigo.tailormoons.sniperelite.gameobjects;

public class ArmoredEnemy extends Enemy {

    private int armour;
    private static final String NAME = "HEAVY_SOLDIER";

    public ArmoredEnemy(String name, int health, int armour) {
        super(name, health);
        this.armour = armour;
    }

    @Override
    public void hit(int damage) {

        if (!getIsDead()) {

            if (armour > 0) {
                armour = armour - damage;
                System.out.println(getName() + " has " + armour + " remaining armour");
                return;
            } else if (getHealth() > 0) {
                super.hit(damage);
                return;
            }
            setIsDead(true);
            System.out.println(getName() + " is dead");
        }
    }

    @Override
    public String getName() {
        return NAME;
    }


}
