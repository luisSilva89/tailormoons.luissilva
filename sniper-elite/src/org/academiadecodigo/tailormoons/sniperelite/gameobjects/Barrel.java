package org.academiadecodigo.tailormoons.sniperelite.gameobjects;

public class Barrel extends GameObject implements Destroyable {

    public String barrelType;
    public int hitPoints;
    public boolean destroyed;


    public Barrel(String barrelType, int hitPoints) {
        this. barrelType = barrelType;
        this.hitPoints = hitPoints;
    }


    @Override
    public boolean isDestroyed() {
        return destroyed;
    }

    @Override
    public void hit(int damage) {
        hitPoints = hitPoints - damage;
        System.out.println(barrelType + " has " + hitPoints + " hit points left" );
            if(hitPoints <= 0) {
                System.out.println("Barrel is destroyed");
                destroyed = true;
            }
    }

    @Override
    public String getMessage() {
        return "A " + barrelType + " has appeared!";
    }


}
