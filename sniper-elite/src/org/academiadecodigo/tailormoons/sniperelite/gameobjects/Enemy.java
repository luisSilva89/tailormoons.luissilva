package org.academiadecodigo.tailormoons.sniperelite.gameobjects;

public abstract class Enemy extends GameObject implements Destroyable{

    private int health;
    private String name;
    private boolean isDead;

    public Enemy(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public void hit(int damage) {

        if(isDead) {
            return;
        }
        health = health - damage;
        System.out.println(getName() + " has " + getHealth() + " remaining health");

        if (health <= 0) {
            isDead = true;
            System.out.println(getName() + " is dead");
        }
    }

    @Override
    public boolean isDestroyed() {
        return isDead;
    }


    @Override
    public String getMessage() {
        return "A " + getName() + " has appeared! Take him out";
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public void setIsDead (boolean isDead){
        this.isDead = isDead;
    }

    public boolean getIsDead() {
        return isDead;
    }


}
