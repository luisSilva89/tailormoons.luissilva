package org.academiadecodigo.tailormoons.sniperelite;
import org.academiadecodigo.tailormoons.sniperelite.gameobjects.*;

public class Game {

    private GameObject[] gameObjects;
    private SniperRifle sniperRifle = new SniperRifle(10);
    private int numberOfObjects;
    private int counter = 0;

    public Game(int numberOfObjects) {
        this.numberOfObjects = numberOfObjects;
    }


    public GameObject[] createObjects(int health, int armour) {
        gameObjects = new GameObject[numberOfObjects];

        for (int i = 0; i < gameObjects.length; i++) {
            int number = (int) Math.ceil(Math.random() * 4);

            if (number == 1) {
                gameObjects[i] = new Tree();
                continue;
            } else if (number == 2) {
                gameObjects[i] = new SoldierEnemy("FOOT_SOLDIER", health);
                continue;
            }else if (number == 3) {
                gameObjects[i] = new ArmoredEnemy("HEAVY_SOLDIER", health, armour);
                continue;
            }
            gameObjects[i] = new Barrel("Metal Barrel", 20);

        }
        return gameObjects;

    }


    public void start() {

        System.out.println("<<Game is starting. Get ready to shoot!>>");
        System.out.println();

        for(int i = 0; i < gameObjects.length; i++) {

            if (!checkIfDestroyable(gameObjects[i])) {
                counter++;
                System.out.println(counter + " << " + gameObjects[i].getMessage() + " >>");
                System.out.println();
                continue;
            }
            if (!checkIfDestroyed((Destroyable)gameObjects[i])) {
                counter++;
                System.out.println(counter + " << " + gameObjects[i].getMessage() + " >>");
                sniperRifle.shoot((Destroyable) gameObjects[i]);
                System.out.println();
            }
        }

        System.out.println(sniperRifle.getBulletsFired() + " of shots were fired. Pick up the shells now.");
    }

    public boolean checkIfDestroyable(GameObject target) {

        if (target instanceof Tree) {
            return false;
        }
        return true;
    }

    private boolean checkIfDestroyed(Destroyable enemy) {
        if(enemy.isDestroyed()) {
            return true;
        }
        return false;
    }

}
