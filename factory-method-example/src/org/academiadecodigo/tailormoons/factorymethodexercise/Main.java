package org.academiadecodigo.tailormoons.factorymethodexercise;

import org.academiadecodigo.tailormoons.factorymethodexercise.factory.PlayerFactory;
import org.academiadecodigo.tailormoons.factorymethodexercise.factory.VideoPlayer;

public class Main {

    public static void main(String[] args) {

        String fileName = "Inception.wav";
        PlayerFactory factory = new PlayerFactory();
        VideoPlayer player = factory.getVideoPlayer(fileName);
        if (player != null) {
            player.play(fileName);
            System.out.println("Cinema session successful");
        }
    }
}

