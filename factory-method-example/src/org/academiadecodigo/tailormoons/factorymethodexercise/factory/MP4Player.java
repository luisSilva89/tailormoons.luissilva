package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public class MP4Player implements VideoPlayer {

    @Override
    public void play(String fileName) {
        System.out.println("play MP4 file " + fileName);
    }
}
