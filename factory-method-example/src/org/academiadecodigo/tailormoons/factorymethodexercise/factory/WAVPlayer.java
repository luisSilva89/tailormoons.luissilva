package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public class WAVPlayer implements VideoPlayer {

    @Override
    public void play(String fileName) {
        System.out.println("play WAV file " + fileName);
    }
}
