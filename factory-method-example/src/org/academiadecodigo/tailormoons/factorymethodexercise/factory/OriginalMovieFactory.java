package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public class OriginalMovieFactory {

    public VideoPlayer getVideoPlayer(String fileName) {

        if (fileName.endsWith(".mp4")) {
            return new MP4Player();
        } else if (fileName.endsWith(".avi")) {
            return new AVIPlayer();
        } else if (fileName.endsWith(".wav")) {
            return new WAVPlayer();
        } else {
            return null;
        }
    }

}
