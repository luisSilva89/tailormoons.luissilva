package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public interface VideoPlayer {

    void play(String fileName);

}
