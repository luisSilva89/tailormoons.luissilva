package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public class FLACPlayer implements VideoPlayer {

    @Override
    public void play(String fileName) {
        System.out.println("play FLAC file " + fileName);
    }
}
