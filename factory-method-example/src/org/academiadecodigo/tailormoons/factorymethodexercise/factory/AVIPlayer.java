package org.academiadecodigo.tailormoons.factorymethodexercise.factory;

public class AVIPlayer implements VideoPlayer {

    @Override
    public void play(String fileName) {
        System.out.println("play AVI file " + fileName);
    }
}
