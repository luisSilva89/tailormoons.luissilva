package org.academiadecodigo.tailormoons.wordreader;

import java.io.*;
import java.util.Iterator;

public class WordReader implements Iterable<String> {

    private String fileName;

    public WordReader(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public Iterator<String> iterator() {
        return new WordIterator();
    }


    public class WordIterator implements Iterator<String> {

        private FileReader reader;
        private BufferedReader bReader = null;
        private String newLine;
        private String[] words;
        private int index = -1;


        public WordIterator() {

            try {
                reader = new FileReader(fileName);
                bReader = new BufferedReader(reader);
                newLine = bReader.readLine();
                words = newLine.split(" ");


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("File not readable or non-existent");

            } catch (IOException e) {
                e.printStackTrace();

            }
        }


        @Override
        public boolean hasNext() {

            if (index + 1 >= words.length) {

                try {

                    newLine = bReader.readLine();
                    if (newLine == null) {
                        return false;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                words = newLine.split(" ");
                index = - 1;
                return true;
            }
            return true;
        }

        @Override
        public String next() {
            index++;
            return words[index];

        }
    }
}




