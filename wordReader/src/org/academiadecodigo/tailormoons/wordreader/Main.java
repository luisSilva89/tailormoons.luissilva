package org.academiadecodigo.tailormoons.wordreader;

public class Main {

    public static void main(String[] args) {


        WordReader wordReader = new WordReader("book");


        for (String word: wordReader) {
            System.out.println(word);
        }
    }
}
