SET FOREIGN_KEY_CHECKS = 0;

DROP table books;
DROP table users;
DROP table loans;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE books(
	id INTEGER NOT NULL AUTO_INCREMENT UNIQUE,
	author CHAR(25) NOT NULL,
	title CHAR(25) NOT NULL,
	publish_date INTEGER NOT NULL,
	publisher CHAR (20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE users(
	cc INTEGER NOT NULL UNIQUE,
	name CHAR(20) NOT NULL,
	birthdate DATE NOT NULL,
	residence CHAR(20) NOT NULL,
	PRIMARY KEY (cc)
);


CREATE TABLE loans(
	id INTEGER NOT NULL AUTO_INCREMENT UNIQUE,
	user_cc INTEGER NOT NULL,
	book_id INTEGER NOT NULL,
	loan_date DATE,
	expire_date DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (user_cc) REFERENCES users(cc),
	FOREIGN KEY (book_id) REFERENCES books(id)
);

INSERT INTO books(id, author, title, publish_date, publisher)
VALUES  (1, "Leo Tolstoy", "War and Peace", 1950, "Antigona"),
	(2, "George Orwell", "1984", 1987, "Antigona"),
	(3, "Miguel de Cervantes", "Don Quixote", 1920, "Bertrand"),
	(4, "F. Scott Fitzgerald", "The Great Gatsby", 1943, "Bertrand"),
	(5, "JRR Tolkien", "Lord of The Rings", 1973, "Antigona")
	;

INSERT INTO users(cc, name, birthdate, residence)
VALUES	(123, "Luis", str_to_date('08-08-1989','%m-%d-%Y'), "Braga"),
	(321, "Manuel", str_to_date('07-07-1975','%m-%d-%Y'), "Braga"),
	(456, "Antonio", str_to_date('09-06-2012','%m-%d-%Y'), "Porto"),
	(654, "Teresa", str_to_date('03-03-2012','%m-%d-%Y'), "V.V."),
	(789, "Joaquim", str_to_date('09-12-2012','%m-%d-%Y'), "Porto")
	;

INSERT INTO loans(id, user_cc, book_id, loan_date, expire_date)
VALUES	(1, 123, 1, NULL, NULL),
	(2, 123, 2, NULL, NULL),
	(3, 321, 4, NULL, NULL),
	(4, 654, 5, NULL, NULL)
	;

//SELECT * FROM books WHERE author = "Leo Tolstoy";

//







