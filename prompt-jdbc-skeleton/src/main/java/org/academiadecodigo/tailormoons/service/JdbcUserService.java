package org.academiadecodigo.tailormoons.service;

import org.academiadecodigo.tailormoons.model.User;
import org.academiadecodigo.tailormoons.persistence.ConnectionManager;
import org.academiadecodigo.tailormoons.utils.Security;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class JdbcUserService implements UserService {


    private List<User> userList = new LinkedList<>();
    private final ConnectionManager connectionManager = new ConnectionManager();
    private final Connection dbConnection = connectionManager.getConnection();
    private User user;


    @Override
    public boolean authenticate(String username, String password) {

        PreparedStatement statement = null;

        String query = "SELECT password FROM user WHERE username=?";

        try {
            statement = dbConnection.prepareStatement(query);

            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String passwordValue = resultSet.getString("password");

                if (passwordValue.equals(Security.getHash(password))) {
                    return true;
                }

            }
            statement.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public void add(User user) {

        Statement statement = null;

        String usernameValue = user.getUsername();
        String passwordValue = user.getPassword();
        String emailValue = user.getEmail();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String phone = user.getPhone();

        String query = "INSERT INTO user(username, password, email, firstname, lastname, phone) VALUES(" +
                "'" + usernameValue + "'," +
                "'" + passwordValue + "'," +
                "'" + emailValue + "'," +
                "'" + firstName + "'," +
                "'" + lastName + "'," +
                "'" + phone + "')";

        try {
            statement = dbConnection.createStatement();
            statement.executeUpdate(query);

            statement.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public User findByName(String username) {

        PreparedStatement statement = null;

        String query = "SELECT * FROM user WHERE username=?";

        // ... connection and statements....
        // execute the query
        try {
            statement = dbConnection.prepareStatement(query);

            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();

            // user exists
            if (resultSet.next()) {

                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String phone = resultSet.getString("phone");

                user = new User(usernameValue, passwordValue, emailValue, firstName, lastName, phone);

                statement.close();

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return user;
    }

    @Override
    public List<User> findAll() {

        Statement statement = null;

        String query = "SELECT * FROM user";

        try {
            statement = dbConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            // get the results
            while (resultSet.next()) {

                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String phone = resultSet.getString("phone");

                user = new User(usernameValue, passwordValue, emailValue, firstName, lastName, phone);
                userList.add(user);

            }
            statement.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return userList;

    }

    @Override
    public int count() {
        int result = 0;

        Statement statement = null;

        // create a query
        String query = "SELECT COUNT(*) FROM user";

        // create a new statement
        // execute the query
        try {
            statement = dbConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            // get the results
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

            statement.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return result;
    }
}
