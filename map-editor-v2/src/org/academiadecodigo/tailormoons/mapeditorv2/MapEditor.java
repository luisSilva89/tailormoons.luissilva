package org.academiadecodigo.tailormoons.mapeditorv2;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.tailormoons.mapeditorv2.grid.Grid;
import org.academiadecodigo.tailormoons.mapeditorv2.grid.Pointer;

public class MapEditor {

    private Grid grid;
    private Pointer pointer;


    public void init(int rows, int cols) {
        grid = new Grid(rows, cols);
        grid.init();

        pointer = new Pointer(rows - 1, cols - 1);

        KeyboardListener keyboardListener = new KeyboardListener(this);
        keyboardListener.init();

    }


    public void pressed(int key) {
        switch (key) {
            case KeyboardEvent.KEY_UP:
                pointer.moveUp();
                break;
            case KeyboardEvent.KEY_DOWN:
                pointer.moveDown();
                break;
            case KeyboardEvent.KEY_LEFT:
                pointer.moveLeft();
                break;
            case KeyboardEvent.KEY_RIGHT:
                pointer.moveRight();
                break;
            case KeyboardEvent.KEY_SPACE:
                grid.paint(pointer.getCol(), pointer.getRow());
                break;
            /*case KeyboardEvent.KEY_S:
                fileHelper.save(grid.toString());
                break;
            case KeyboardEvent.KEY_L:
                grid.load(fileHelper.load());
                break;*/
            case KeyboardEvent.KEY_C:
                grid.clear();
                break;
            case KeyboardEvent.KEY_1:
                grid.changeColor(Color.BLACK);
                break;
            case KeyboardEvent.KEY_2:
                grid.changeColor(Color.BLUE);
                break;
            case KeyboardEvent.KEY_3:
                grid.changeColor(Color.CYAN);
                break;
            case KeyboardEvent.KEY_4:
                grid.changeColor(Color.ORANGE);
                break;
            case KeyboardEvent.KEY_5:
                grid.changeColor(Color.GREEN);
                break;
            case KeyboardEvent.KEY_6:
                grid.changeColor(Color.MAGENTA);
                break;
            case KeyboardEvent.KEY_7:
                grid.changeColor(Color.PINK);
                break;
            case KeyboardEvent.KEY_8:
                grid.changeColor(Color.RED);
                break;
            case KeyboardEvent.KEY_9:
                grid.changeColor(Color.LIGHT_GRAY);
                break;
        }
    }


}
