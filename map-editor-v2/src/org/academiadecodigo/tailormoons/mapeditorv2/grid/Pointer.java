package org.academiadecodigo.tailormoons.mapeditorv2.grid;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;


public class Pointer {


    private Rectangle representation;
    private int col;
    private int row;
    private int maxRow;
    private int maxCol;

    public Pointer(int maxRow, int maxCol) {
        representation = new Rectangle(Grid.PADDING, Grid.PADDING, Cell.SIZE, Cell.SIZE);
        representation.setColor(Color.ORANGE);
        representation.fill();
        this.maxRow = maxRow;
        this.maxCol = maxCol;

    }

    public void moveUp() {
        if (row > 0) {
            representation.translate(0, -Cell.SIZE);
            row--;
        }
    }

    public void moveDown() {
        if (row < maxRow) {
            representation.translate(0, Cell.SIZE);
            row++;
        }
    }

    public void moveLeft() {
        if (col > 0) {
            representation.translate(-Cell.SIZE, 0);
            col--;
        }
    }

    public void moveRight() {
        if (col < maxCol) {
            representation.translate(Cell.SIZE, 0);
            col++;
        }
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }


}




