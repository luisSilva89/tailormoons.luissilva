package org.academiadecodigo.tailormoons.mapeditorv2.grid;


import org.academiadecodigo.simplegraphics.graphics.Color;

public class Grid {


    public static final int PADDING = 10;
    private Cell[][] cells;
    private int cols;
    private int rows;


    public Grid(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
    }


    public void init() {

        cells = new Cell[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col] = new Cell(row, col);
                cells[row][col].draw();

            }
        }
    }


    public void paint(int col, int row) {
        if (!cells[row][col].getIsPainted()) {
            cells[row][col].paint();
            return;
        }
        cells[row][col].draw();
    }


    public void clear() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col].draw();
            }
        }
    }


    public void changeColor(Color color) {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col].setColor(color);
            }
        }
    }


}
