package org.academiadecodigo.tailormoons.mapeditorv2.grid;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {

    private Rectangle representation;
    private boolean isPainted;
    private Color color;
    private int col;
    private int row;


    public static final int SIZE = 20;


    public Cell(int row, int col) {
        representation = new Rectangle(coordinateToPixel(col), coordinateToPixel(row), SIZE, SIZE);
    }

    public void draw() {
        representation.draw();
        isPainted = false;
    }

    public void paint() {
        representation.fill();
        isPainted = true;
    }

    public int coordinateToPixel(int coordinate) {
        return coordinate * SIZE + Grid.PADDING;
    }

    public boolean getIsPainted() {
        return isPainted;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
