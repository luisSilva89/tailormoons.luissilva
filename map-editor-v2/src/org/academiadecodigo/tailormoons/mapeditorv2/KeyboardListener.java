package org.academiadecodigo.tailormoons.mapeditorv2;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class KeyboardListener implements KeyboardHandler {


    private static int[] KEY_CODES = {
            KeyboardEvent.KEY_SPACE,
            KeyboardEvent.KEY_UP,
            KeyboardEvent.KEY_DOWN,
            KeyboardEvent.KEY_LEFT,
            KeyboardEvent.KEY_RIGHT,
            KeyboardEvent.KEY_C,
            KeyboardEvent.KEY_1,
            KeyboardEvent.KEY_2,
            KeyboardEvent.KEY_3,
            KeyboardEvent.KEY_4,
            KeyboardEvent.KEY_5,
            KeyboardEvent.KEY_6,
            KeyboardEvent.KEY_7,
            KeyboardEvent.KEY_8,
            KeyboardEvent.KEY_9,
    };

    private MapEditor mapEditor;

    public KeyboardListener(MapEditor mapEditor) {
        this.mapEditor = mapEditor;
    }

    public void init() {
        Keyboard keyboard = new Keyboard(this);

        for (int code : KEY_CODES) {
            subscribe(keyboard, code);
        }
    }

    private void subscribe(Keyboard keyboard, int code) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        event.setKey(code);
        keyboard.addEventListener(event);
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        mapEditor.pressed(keyboardEvent.getKey());

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
