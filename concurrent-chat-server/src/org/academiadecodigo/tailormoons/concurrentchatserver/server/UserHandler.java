package org.academiadecodigo.tailormoons.concurrentchatserver.server;


import org.academiadecodigo.tailormoons.concurrentchatserver.server.User;

import java.util.LinkedList;

public class UserHandler {


    private LinkedList<User> usersList = new LinkedList<>();


    public void addUser(User user) {

        synchronized (usersList) {

            usersList.add(user);

        }
    }

    public void removeUser(User user) {

        synchronized (usersList) {
            usersList.remove(user);
        }
    }

    public void broadcast(String message, String name) {

        synchronized (usersList) {

            for (User user : usersList) {

                user.send(name + message);

            }
        }
    }



}
