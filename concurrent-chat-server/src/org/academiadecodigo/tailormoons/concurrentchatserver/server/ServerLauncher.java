package org.academiadecodigo.tailormoons.concurrentchatserver.server;

import org.academiadecodigo.tailormoons.concurrentchatserver.server.Server;

import java.io.IOException;

public class ServerLauncher {


    public static void main(String[] args) {


        try {
            Server server = new Server(8080);
            server.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
