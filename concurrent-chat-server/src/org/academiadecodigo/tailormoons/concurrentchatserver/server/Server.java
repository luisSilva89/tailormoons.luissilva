package org.academiadecodigo.tailormoons.concurrentchatserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {


    private ServerSocket serverSocket;
    private UserHandler userHandler;
    private int i;


    public Server(int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber);
        userHandler = new UserHandler();

    }

    public void start() {

        ExecutorService service = Executors.newFixedThreadPool(10);

        while (true) {

            try {
                Socket clientSocket = serverSocket.accept();
                String name = generateName();
                User user = new User(name, clientSocket, userHandler);

                userHandler.addUser(user);

                service.execute(user);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public String generateName() {

        String defaultName = ("user<" + i + ">:");
        i++;
        return defaultName;
    }


}
