package org.academiadecodigo.tailormoons.concurrentchatserver.server.commands;

import org.academiadecodigo.tailormoons.concurrentchatserver.server.User;
import org.academiadecodigo.tailormoons.concurrentchatserver.server.UserHandler;

public class ChangeNameHandler implements CommandHandler {


    @Override
    public void execute(User user, UserHandler userHandler, String text) {

        userHandler.broadcast(" changed its name to " + text, user.getName());
        user.changeName(text);
    }


}
