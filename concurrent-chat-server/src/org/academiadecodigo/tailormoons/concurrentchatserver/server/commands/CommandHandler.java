package org.academiadecodigo.tailormoons.concurrentchatserver.server.commands;


import org.academiadecodigo.tailormoons.concurrentchatserver.server.User;
import org.academiadecodigo.tailormoons.concurrentchatserver.server.UserHandler;

public interface CommandHandler {


    void execute(User user, UserHandler userHandler, String text);

}
