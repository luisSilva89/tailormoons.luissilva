package org.academiadecodigo.tailormoons.concurrentchatserver.server;

import org.academiadecodigo.tailormoons.concurrentchatserver.server.commands.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.TreeMap;

public class User implements Runnable {

    private Socket clientSocket;
    private BufferedReader in;
    private PrintWriter out;
    private String name;
    private UserHandler userHandler;
    private Map<String, CommandHandler> handlers = new TreeMap<>();


    public User(String name, Socket clientSocket, UserHandler userHandler) {
        this.clientSocket = clientSocket;
        this.name = name;
        this.userHandler = userHandler;
    }

    private void init() {
        handlers.put("/name", new ChangeNameHandler());
        handlers.put("/list", new ListUsersHandler());
        handlers.put("/whisper", new WhisperHandler());
        handlers.put("/quit", new QuitHandler());
    }

    @Override
    public void run() {

        init();

        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        out.println("Welcome to a random chat!");

        while (!clientSocket.isClosed()) {

            try {

                String message = in.readLine();

                if (message == null) {
                    String dcMessage = name + " <has disconnected>";
                    userHandler.broadcast(dcMessage, name);
                    clientSocket.close();
                    userHandler.removeUser(this);
                    return;

                } else if (message.startsWith("/")) {

                    String command = message.split(" ")[0];
                    String argument = message.split(" ")[1];

                    //Strategy Pattern usage
                    handlers.getOrDefault(command, new InvalidCommandHandler()).execute(this, userHandler, argument);
                }else userHandler.broadcast(message,name);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void changeName(String newName) {
        this.name = newName + ":";
    }

    public void send(String message) {
        out.println(message);
    }

    public String getName() {
        return name;
    }

}
