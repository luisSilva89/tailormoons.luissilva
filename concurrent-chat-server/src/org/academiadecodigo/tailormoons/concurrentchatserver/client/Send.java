package org.academiadecodigo.tailormoons.concurrentchatserver.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Send implements Runnable{

    private final Scanner scanner;
    private PrintWriter out;
    private final Socket clientSocket;


    public Send(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        scanner = new Scanner(System.in);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
    }



    @Override
    public void run() {

        while (!clientSocket.isClosed()) {

            String message = readInput();
            send(message);

            if(message.startsWith("/quit")) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String readInput() {
        return scanner.nextLine();
    }

    private void send(String message) {
        out.println(message);
    }

}
