package org.academiadecodigo.tailormoons.concurrentchatserver.client;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private final Socket clientSocket;



    public Client(String hostname, int portNumber) throws IOException {

        clientSocket = new Socket(hostname, portNumber);
    }


    public void start() {

        try{

            Thread thread = new Thread(new Receive(clientSocket));
            thread.start();
            Send send = new Send(clientSocket);
            send.run();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
