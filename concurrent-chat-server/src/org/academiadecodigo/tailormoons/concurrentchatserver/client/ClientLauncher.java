package org.academiadecodigo.tailormoons.concurrentchatserver.client;

import org.academiadecodigo.tailormoons.concurrentchatserver.client.Client;

import java.io.*;
import java.net.SocketException;
import java.net.UnknownHostException;



public class ClientLauncher {


    private static final String USAGE_MESSAGE = "Usage: java ClientLauncher <host> <port>";


    public static void main(String[] args) {


        if (args.length < 2) {
            System.out.println(USAGE_MESSAGE);
            return;
        }

        try {

            String host = args[0];
            int portNumber = Integer.parseInt(args[1]);

            Client client = new Client(host, portNumber);
            client.start();


        } catch (NumberFormatException e) {
            System.out.println("Invalid port number: " + args[0]);
            System.out.println(USAGE_MESSAGE);
        } catch (SocketException e) {
            System.err.println("Error opening socket: " + e.getMessage());
        } catch (UnknownHostException e) {
            System.err.println("Invalid host name: " + args[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
