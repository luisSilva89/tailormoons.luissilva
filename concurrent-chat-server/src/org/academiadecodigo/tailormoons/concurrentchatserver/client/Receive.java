package org.academiadecodigo.tailormoons.concurrentchatserver.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Receive implements Runnable{


    private BufferedReader in;
    private PrintWriter out;
    private Socket clientSocket;


    public Receive(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }


    @Override
    public void run() {

        while(!clientSocket.isClosed()) {

            try {
                String message = in.readLine();
                System.out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
