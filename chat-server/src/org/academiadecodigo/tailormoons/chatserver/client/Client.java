package org.academiadecodigo.tailormoons.chatserver.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {


    private final String CLOSE_MESSAGE = "/quit";

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private Scanner scanner;


    public Client(String serverHostname, int serverPort) throws IOException {
        clientSocket = new Socket(serverHostname, serverPort);
        scanner = new Scanner(System.in);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new PrintWriter(clientSocket.getOutputStream(), true);
    }


    public void start() throws IOException {

        while (!clientSocket.isClosed()) {

            try {
                String message = getUserInput();

                if (message.equals(CLOSE_MESSAGE)) {
                    clientSocket.close();
                    continue;
                }

                send(message);
                receive();

            } catch (IOException e) {
                System.err.println("Error communicating with server: " + e.getMessage());
                clientSocket.close();
            }
        }
    }

    private String getUserInput() {
        System.out.print("Input your message: ");
        return scanner.nextLine();
    }

    private void send(String message) throws IOException {

        out.println(message);
    }

    private void receive() throws IOException {

        System.out.println(in.readLine());
    }
}


