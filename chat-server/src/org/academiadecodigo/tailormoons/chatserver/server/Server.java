package org.academiadecodigo.tailormoons.chatserver.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {


    private int portNumber;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String message = "";


    public Server(int portNumber) throws IOException {

        ServerSocket serverSocket = new ServerSocket(portNumber);
        clientSocket = serverSocket.accept();
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new PrintWriter(clientSocket.getOutputStream(), true);
    }


    public void start() throws IOException {

        while (clientSocket != null) {

            receive();
        }
        close();
    }

    public void receive() throws IOException {

        while ((message = in.readLine()) != null) {
            System.out.println(message);
            send();
        }
    }

    public void send() {
        out.println(message.toUpperCase());
    }

    public void close() throws IOException {
        in.close();
        out.close();
        serverSocket.close();
    }

}
