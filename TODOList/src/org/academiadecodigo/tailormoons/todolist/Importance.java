package org.academiadecodigo.tailormoons.todolist;

public enum Importance {

    HIGH(3),
    MEDIUM(2),
    LOW(1);

    private int value;

    Importance(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


}
