package org.academiadecodigo.tailormoons.todolist;

public class Task implements Comparable<Task> {


    private Importance importance;
    private int priority;
    private String task;


    public Task(Importance importance, int priority, String task) {
        this.importance = importance;
        this.priority = priority;
        this.task = task;
    }

    public Importance getImportance() {
        return importance;
    }

    public int getPriority() {
        return priority;
    }


    @Override
    public int compareTo(Task o) {

        if (this.importance.getValue() > o.getImportance().getValue()) {
            return -1;
        } else if (this.importance.getValue() < o.getImportance().getValue()) {
            return 1;
        }
        if (this.priority > o.getPriority()) {
            return 1;
        } else if (this.priority < o.getPriority()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Task of importance " + importance + " of priority " + priority + " -> " + task;
    }
}
