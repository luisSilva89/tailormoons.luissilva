package org.academiadecodigo.tailormoons.todolist;

public class Main {

    public static void main(String[] args) {


        TodoList todoList = new TodoList();

        todoList.add(Importance.HIGH, 1, "Study Study Study");
        todoList.add(Importance.HIGH, 2, "Study some more");
        todoList.add(Importance.HIGH, 3, "Study again");

        todoList.add(Importance.MEDIUM, 1, "Rest");
        todoList.add(Importance.MEDIUM, 1, "Take a nap");

        todoList.add(Importance.LOW, 1, "Relax");


        while (!todoList.isEmpty()) {
            System.out.println(todoList.remove());
        }

    }

}
