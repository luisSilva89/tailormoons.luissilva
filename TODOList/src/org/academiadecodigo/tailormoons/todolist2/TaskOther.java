package org.academiadecodigo.tailormoons.todolist2;

import org.academiadecodigo.tailormoons.todolist.Importance;

public class TaskOther {


    private Importance importance;
    private int priority;
    private String task;


    public TaskOther(Importance importance, int priority, String task) {
        this.importance = importance;
        this.priority = priority;
        this.task = task;
    }

    public Importance getImportance() {
        return importance;
    }

    public int getPriority() {
        return priority;
    }


    @Override
    public String toString() {
        return "Task of importance " + importance + " of priority " + priority + " -> " + task;
    }
}


