package org.academiadecodigo.tailormoons.todolist2;

import org.academiadecodigo.tailormoons.todolist.Importance;
import org.academiadecodigo.tailormoons.todolist.Task;

import java.util.Comparator;
import java.util.PriorityQueue;

public class TodoListOther {

    private PriorityQueue<TaskOther> queue;

    public TodoListOther() {
        Comparator comp = new CompareTasks();
        this.queue = new PriorityQueue(comp);
    }


    public void add(Importance importance, int priority, String task) {
        queue.add(new TaskOther(importance, priority, task));

    }

    public TaskOther remove() {
        return queue.poll();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    private class CompareTasks implements Comparator<TaskOther> {


        @Override
        public int compare(TaskOther o1, TaskOther o2) {
            if (o1.getImportance().getValue() > o2.getImportance().getValue()) {
                return -1;
            } else if (o1.getImportance().getValue() < o2.getImportance().getValue()) {
                return 1;
            }
            if (o1.getPriority() > o2.getPriority()) {
                return 1;
            } else if (o1.getPriority() < o2.getPriority()) {
                return -1;
            }
            return 0;

        }

    }

}
